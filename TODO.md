# bit-rest TODO list

* Improve formatting
  * Add conditional formatting
  * Add color codes
  * Deal with time/date locales

* Implement Bitbucket REST resources:
  * Changesets
  * Emails
  * Events
  * Followers
  * Groups
  * Groups Privileges
  * Invitations
  * Issues - DONE
    * Issue Comments
    * Issue Components
    * Issue Milestones
    * Issue Versions
  * Privileges
  * Repositories
  * Services
  * Source
  * SSH Keys
  * Users
  * Wiki
