# bit-rest

## Description
[Bitbucket][bb] client with command line interface written in perl.

It's written to get a quick and convinient access to Bitbucket repositories.
The aim is to work with a repo without a browser using a command line only.

## Documentation
* [Bitbucket REST API](https://confluence.atlassian.com/display/BITBUCKET/Using+the+bitbucket+REST+APIs);
  * [Changesets](https://confluence.atlassian.com/display/BITBUCKET/Changesets "Changesets");
  * [Emails](https://confluence.atlassian.com/display/BITBUCKET/Emails "Emails");
  * [Events](https://confluence.atlassian.com/display/BITBUCKET/Events "Events");
  * [Followers](https://confluence.atlassian.com/display/BITBUCKET/Followers "Followers");
  * [Groups](https://confluence.atlassian.com/display/BITBUCKET/Groups "Groups");
  * [Groups Privileges](https://confluence.atlassian.com/display/BITBUCKET/Groups+Privileges "Groups Privileges");
  * [Invitations](https://confluence.atlassian.com/display/BITBUCKET/Invitations "Invitations");
  * [Issues](https://confluence.atlassian.com/display/BITBUCKET/Issues);
    * [Issue Comments](https://confluence.atlassian.com/display/BITBUCKET/Issue+Comments "Issue Comments");
    * [Issue Components](https://confluence.atlassian.com/display/BITBUCKET/Issue+Components "Issue Components");
    * [Issue Milestones](https://confluence.atlassian.com/display/BITBUCKET/Issue+Milestones "Issue Milestones");
    * [Issue Versions](https://confluence.atlassian.com/display/BITBUCKET/Issue+Versions "Issue Versions");
  * [Privileges](https://confluence.atlassian.com/display/BITBUCKET/Privileges "Privileges");
  * [Repositories](https://confluence.atlassian.com/display/BITBUCKET/Repositories);
  * [Services](https://confluence.atlassian.com/display/BITBUCKET/Services "Services");
  * [Source](https://confluence.atlassian.com/display/BITBUCKET/Source "Source");
  * [SSH Keys](https://confluence.atlassian.com/display/BITBUCKET/SSH+Keys "SSH Keys");
  * [Users](https://confluence.atlassian.com/display/BITBUCKET/Users "Users");
  * [Wiki](https://confluence.atlassian.com/display/BITBUCKET/Wiki);
* [REST][];
* [libcurl][curl];
* [libcurl wikia][curl2];
* [json][];
* [Perldoc][].

[bb]: https://bitbucket.org/

[rest]: https://ru.wikipedia.org/wiki/REST "w:REST"
[curl]: http://curl.haxx.se/libcurl/ "libcurl documentation"
[curl2]: http://ru.libcurl.wikia.com/ "libcurl wikia"
[json]: http://www.json.org/ "JSON"

[perl]: http://www.perl.org/ "Perl"
[perldoc]: http://perldoc.perl.org/ "Perl Documentation"
