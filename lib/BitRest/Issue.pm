package BitRest::Issue;
# vim: set ft=perl et sw=4 ts=4:

=head1 NAME

BitRest::Issue - Perl object interface to Bitbucket issue tracker.

=head1 VERSION

Version 0.43

=cut

our $VERSION = '0.43';

=head1 SYNOPSIS

B<bit-rest> I<command> B<issue> [I<id>] [I<options>]

=head2 Commands

B<get>, B<add>, B<put>, B<del>

=head1 DESCRIPTION

BitRest::Issue inherits BitRest and implements the Bitbucket REST resource
API for issue tracker.

=head1 OPTIONS

=head2 Options for C<get> command only

=over 4

=item B<-f>, B<--format-short> I<format>

Specify short output I<format> used for one-line view. See L</FORMAT>.

=item B<-F>, B<--format-long> I<format>

Specify long output I<format> used for full view. See L</FORMAT>.

=item B<-s>, B<--search> I<string>

Specify search query.

=item B<-A>, B<--reported_by> I<string ...>

Search issue by author.

=item B<-a>, B<--start> I<integer>

Specify offset to start at. The default is B<0>.

=item B<-l>, B<--limit> I<integer>

Specify maximum number of issues returned. The default is B<15>.

=back

=head2 Issue Parameters / Filter Criteria (for B<get>, B<add>, or B<put>)

=over 4

=item B<-t>, B<--title> I<string ...>

Specify title of issue.

=item B<-C>, B<--content> I<string ...>

Specify content of issue.

=item B<-c>, B<--component> I<string ...>

Specify component associated with issue.

=item B<-m>, B<--milestone> I<string ...>

Specify milestone associated with issue.

=item B<-v>, B<--version> I<string ...>

Specify version associated with issue.

=item B<-k>, B<--kind> I<string ...>

Specify kind of issue: B<bug>, B<enhancement>, or B<proposal>.

=item B<-S>, B<--status> I<string ...>

Specify status of issue: B<new>, B<open>, B<resolved>, B<on hold>,
B<invalid>, B<duplicate>, or B<wontfix>.

=item B<-R>, B<--responsible> I<string ...>

Specify user responsible for issue.

=back

=head1 FORMAT

The following format specifiers can be used in argument
for B<--format-short> and B<--format-long> options:

=over 4

=item B<%n> - id

=item B<%t> - title

=item B<%c> - content as string

=item B<%k> - kind of issue as symbol

=item B<%K> - kind of issue as string

=item B<%s> - status as symbol

=item B<%S> - status as string

=item B<%p> - priority as symbol

=item B<%P> - priority as string

=item B<%rn> - reported by (username)

=item B<%rf> - reported by (first name)

=item B<%rl> - reported by (last name)

=item B<%RN> - responsible (username)

=item B<%RF> - responsible (first name)

=item B<%RL> - responsible (last name)

=item B<%ct> - created (time)

=item B<%cd> - created (date)

=item B<%ut> - last updated (time)

=item B<%ud> - last updated (date)

=item B<%V> - version

=item B<%C> - component

=item B<%M> - milestone

=item B<%CC> - comments count

=item B<%FC> - followers count

=item B<%SP> - is spam?

=back

=cut

use strict;
use warnings;
use feature 'switch';

our @ISA = ('BitRest');

=head1 METHODS

=over 4

=item B<new>()

Initialize object, set fields to issue-specific values.
Return value is blessed reference to BitRest::Issue or B<undef> if failed.

=cut

sub new {
    my $class = shift;
    my $self  = $class->SUPER::init;
    $self->{LINK}    = 'repositories/%u/%r/issues/%i';
    $self->{ACTIONS} = [qw(get add put del)];
    $self->{FMT_SHORT} = '%4n %k%p%s %-55.55t %ct %cd';
    $self->{FMT_LONG}  = <<FMT;
Issue %4n: %S %P %K by %rn @ %ct %cd %SP
Component: %C; Version: %V; Milestone: %M
%t
%c
FMT
    $self->{OPTIONS} = [
        'format-short|f=s',     'format-long|F=s',
        'search|s=s',           'title|t=s@{1,}',
        'content|C=s@{1,}',     'version|v=s@{1,}',
        'milestone|m=s@{1,}',   'component|c=s@{1,}',
        'kind|k=s@{1,}',        'status|S=s@{1,}',
        'responsible|R=s@{1,}', 'reported_by|A=s@{1,}',
        'start|a=i',            'limit|l=i',
        'open|O'
    ];
    $self->{KIND} = {
        bug         => ["\N{U+2022}", '1;31'],    # red
        enhancement => ["\N{U+2197}", '1;36'],    # cyan
        proposal    => [qw(+ 1;32)],              # green
        task        => [qw(* 1;33)],              # yellow
    };
    $self->{STATUS} = {
        new       => [qw(N)],           # magenta
        open      => [qw(O 1;33)],      # yellow
        resolved  => [qw(R 1;32)],      # green
        "on hold" => [qw(H 1;30)],      # dark gray
        invalid   => [qw(X 31)],        # dark red
        duplicate => [qw(D 1;37;43)],   # white on brown
        wontfix   => [qw(W 33)],        # brown
    };
    $self->{PRIORITY} = {
        trivial  => [' '],                   # normal
        minor    => ["\N{U+2582}", '1;32'],  # green on normal
        major    => ["\N{U+2584}", '1;33'],  # yellow on normal
        critical => ["\N{U+2586}", '1;31'],  # red on normal
        blocker  => [qw(! 1;5;33;41)],       # yellow on red
    };
    return bless $self, $class;
}

=item B<options>(I<$opt>)

Handle command line options. I<$opt> is a reference to a hash
with command line options.

Options to handle: B<format-short>, B<format-long>, B<open>.

=cut

sub options {
    my ( $self, $opt ) = @_;
    if ( defined $opt->{'format-short'} ) {
        $self->{FMT_SHORT} = $opt->{'format-short'};
        delete $opt->{'format-short'};
    }
    if ( defined $opt->{'format-long'} ) {
        $self->{FMT_LONG} = $opt->{'format-long'};
        delete $opt->{'format-long'};
    }
    if ( defined $opt->{open} ) {
        $opt->{status} = [qw(new open)];
        delete $opt->{open};
    }
}

=item B<print>(I<$data>)

Print issue or list of issues. I<$data> is a reference to a hash
with REST resource data.

=cut

sub print {
    my ( $self, $data ) = @_;
    $self->SUPER::print($data, 'issues');
}

sub fmt_choice {
    my ( $self, $n, $c, $arg, $data ) = @_;
    my $t = qr/^.*([0-9]{2}):([0-9]{2}):([0-9]{2}).*$/o;    # time regex
    my $d = qr/^([0-9]{2})([0-9]{2})-([0-9]{2})-([0-9]{2}).*$/o;  # date regex
    given($c){
        when('n'){ $c = 'd'; push @$arg, $data->{local_id}; }
        when('t'){ $c = 's'; push @$arg, $data->{title}; }
        when('c'){ $c = 's'; push @$arg, $data->{content}; }
        when('k'){ $c = 's'; push @$arg, $self->colored( @{ $self->{KIND}{ $data->{metadata}{kind} } } ); }
        when('K'){ $c = 's'; push @$arg, $data->{metadata}{kind}; }
        when('s'){ $c = 's'; push @$arg, $self->colored( @{ $self->{STATUS}{ $data->{status} } } ); }
        when('S'){ $c = 's'; push @$arg, $data->{status}; }
        when('p'){ $c = 's'; push @$arg, $self->colored( @{ $self->{PRIORITY}{ $data->{priority} } } ); }
        when('P'){ $c = 's'; push @$arg, $data->{priority}; }
        when('rn'){ $c = 's'; push @$arg, $data->{reported_by}{username}; }
        when('rf'){ $c = 's'; push @$arg, $data->{reported_by}{first_name}; }
        when('rl'){ $c = 's'; push @$arg, $data->{reported_by}{last_name}; }
        when('RN'){ $c = 's'; push @$arg, $data->{responsible}{username}; }
        when('RF'){ $c = 's'; push @$arg, $data->{responsible}{first_name}; }
        when('RL'){ $c = 's'; push @$arg, $data->{responsible}{last_name}; }
        when('cr'){ $c = 's'; push @$arg, $data->{utc_created_on}; }
        when('ct'){ $c = 's'; push @$arg, $data->{utc_created_on} =~ s/$t/$1:$2/r; }
        when('cd'){ $c = 's'; push @$arg, $data->{utc_created_on} =~ s/$d/$4.$3.$2/r; }
        when('ur'){ $c = 's'; push @$arg, $data->{utc_last_updated}; }
        when('ut'){ $c = 's'; push @$arg, $data->{utc_last_updated} =~ s/$t/$1:$2/r; }
        when('ud'){ $c = 's'; push @$arg, $data->{utc_last_updated} =~ s/$d/$4.$3.$2/r; }
        when('V'){ $c = 's'; push @$arg, $data->{metadata}{version}; }
        when('C'){ $c = 's'; push @$arg, $data->{metadata}{component}; }
        when('M'){ $c = 's'; push @$arg, $data->{metadata}{milestone}; }
        when('CC'){ $c = 'c'; push @$arg, $data->{comment_count}; }
        when('FC'){ $c = 'c'; push @$arg, $data->{follower_count}; }
        when('SP'){
            if ( $data->{is_spam} ){
                $c = 's'; push @$arg, $self->colored('SPAM', '1;5;33;41');
            }
            else {
                $c = $n = ''
            }
        }
        default{ $n = $c = '' }
    }
    return $n.$c;
}

1;

__END__
# vim: set ft=perl et ts=4 sw=4:

=back

=head1 SEE ALSO

L<BitRest>

L<Bitbucket REST APIs: Issues|https://confluence.atlassian.com/display/BITBUCKET/Issues>

=head1 SOURCE

The source code repository for BitRest::Issue can be found at
L<https://bitbucket.org/vvp/bit-rest>.

=head1 BUGS

See the repository issue tracker at
L<https://bitbucket.org/vvp/bit-rest/issues>
to report and view bugs.

=head1 AUTHOR

vvp <vvp.psu[at]gmail.com>

=head1 LICENSE AND COPYRIGHT

Copyright (c) 2012 vvp (vvp.psu[at]gmail.com).
All rights reserved.

This module is free software; you can redstribute it and/or modify it under
the same terms as Perl itself. See L<perlartistic>.  This module is
distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.

