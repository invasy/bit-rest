use strict;
use warnings;
use Test::More tests => 8;

BEGIN {
	use_ok('Config::Tiny');
	use_ok('Getopt::Long');
	use_ok('Term::ReadKey');
	use_ok('Pod::Usage');
}
require_ok('Config::Tiny');
require_ok('Getopt::Long');
require_ok('Term::ReadKey');
require_ok('Pod::Usage');
