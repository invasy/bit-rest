#!/usr/bin/env perl

BEGIN {
	chdir 't';
	unshift @INC, '../lib', './lib';
}

use strict;
use warnings;
use Test::More tests => 19;

require_ok('BitRest::Issue');

TODO: {
	local $TODO = "Not all REST resources are implemented";
	require_ok('BitRest::Changeset');
	require_ok('BitRest::Email');
	require_ok('BitRest::Event');
	require_ok('BitRest::Follower');
	require_ok('BitRest::Group');
	require_ok('BitRest::GPriv');
	require_ok('BitRest::Invitation');
	require_ok('BitRest::Comment');
	require_ok('BitRest::Component');
	require_ok('BitRest::Milestone');
	require_ok('BitRest::Version');
	require_ok('BitRest::Priv');
	require_ok('BitRest::Repo');
	require_ok('BitRest::Service');
	require_ok('BitRest::Source');
	require_ok('BitRest::Key');
	require_ok('BitRest::User');
	require_ok('BitRest::Wiki');
}
